pragma solidity ^0.4.2;

contract Item
{
    string SeizureId;
    string Description;
    struct Track{
        address Name;
        string LocationId;
        string Status;
    }
    string ItemStatus;
    uint StartTimestamp;
    uint EndTimestamp;
    Track[] public tracks;
    mapping(uint => Track) Trail;
    uint Count = 0;

    function Item(string _seizureId , string _desc) {
        SeizureId = _seizureId;
        Description = _desc;
        StartTimestamp = now;
    }

    function AddTrack(string location,string trackStatus) {
        Track memory newTrack;
        newTrack.Name =  msg.sender;
        newTrack.LocationId = location;
        newTrack.Status = trackStatus;
        Trail[Count]=newTrack;
        Count++;
        
    }
    
    function GetItem() returns (string , string ,uint , string) {
        
        return(SeizureId,"BMW",now,"NEW");
    }

    function GetTrailCount() returns (uint) {
        
        return (Count);
    }

    function GetCurrentStatus(uint _count) returns (string,string,address) {
        return (Trail[_count].Status,Trail[_count].LocationId,Trail[_count].Name);
        
     }
    



}