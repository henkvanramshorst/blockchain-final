pragma solidity ^0.4.2;
import "./Item.sol";

contract Seizure
{
    string holder;
    string why;
    string who;
    uint StartTimestamp;
    uint endTimestamp;
    address[] public itemAddress;
    address owner;

    function Seizure() {
        owner = msg.sender;
        StartTimestamp = now;
    }

    function createItemContract(string SeizureId , string desc){
        Item item = new Item(SeizureId,desc);
        itemAddress.push(item);
    }

    function GetItems() returns (address[]){
        return itemAddress;
    }

    



}