import "../stylesheets/app.css";
import { default as Web3} from 'web3';
import { default as contract } from 'truffle-contract';
import { default as CryptoJS} from 'crypto-js';
import seizure_artifacts from '../../build/contracts/Seizure.json'
import item_artifacts from '../../build/contracts/Item.json'

// MetaCoin is our usable abstraction, which we'll use through the code below.
var Item = contract(item_artifacts);
// MetaCoin is our usable abstraction, which we'll use through the code below.
var Seizure = contract(seizure_artifacts);
var accounts;
var account;
var seizureABI;
var seizureContract;
var seizureCode;
var itemCode = [];
window.App = {
  start: function() {
    var self = this;
    Seizure.setProvider(web3.currentProvider);
    Item.setProvider(web3.currentProvider);
    web3.eth.getAccounts(function(err, accs) {
      if (err != null) {
        alert("There was an error fetching your accounts.");
        return;
      }
      if (accs.length == 0) {
        alert("Couldn't get any accounts! Make sure your Ethereum client is configured correctly.");
        return;
      }

      accounts = accs;
      account = accounts[0];
      web3.eth.defaultAccount= account;
      var itemcount = 0;
      var address ;
      Seizure.deployed().then(function(f){
        console.log(f.address);
         f.GetItems.call().then(function(data){
            address = f.address;
         
               
          });

      });
      Item.deployed().then(function(item){
      var item;
      var trackCOunt = 0;
        if(item){
          itemcount++;
          item.GetItem.call().then(function(itemDetails){
            console.log(itemDetails.length);
            var itemX = "<tr><td style='word-wrap: break-word;'>"+address.substring(0,10)+"...</td><td>23-09-2017</td><td>"+itemcount+"</td><td class='highlight'><a href='http://localhost:8081/civdetail.html'>BEKIJK ZAAK ▸<a></td></tr>";
            $("#item-table").append(itemX);
            var items;
            for(var j=0;j<itemcount;j++){
              var itemDetail = "<tr class='header'><td>GEGEVENS</td><td></td></tr><tr><td class='bold title'>DATUM INBESLAGNAME</td><td>"+itemDetails[0]+"</td></tr><tr><td class='bold title'>ITEM</td><td>"+itemDetails[0]+"</td></tr><tr><td class='bold title'>STATUS</td><td id='status'>"+itemDetails[3]+"</td></tr><tr><td class='bold title'>FOTO'S</td><td><img src='img/phone-1.jpg'/><img src='img/phone-2.jpg' /><img src='img/phone-3.jpg' /></td></tr>";
              $("#detail").append(itemDetail);
            }
           
          }).catch(function(e) {
            // There was an error! Handle it.
            console.log(e);
          });
          item.GetTrailCount.call().then(function(d){
              trackCOunt = d.c.length;
              item.GetCurrentStatus.call(trackCOunt-1).then(function(status){
                $("#status").text(status[0]);
                var items;
               
               
              })
          })
         .catch(function(e) {
            // There was an error! Handle it.
            console.log(e);
          });

          
        }else{
          console.log(data[0]);
          item = Item.at(data[0]);
        }
      });
      Item.deployed().then(function(item){
        item.GetTrailCount.call().then(function(d){
          trackCOunt = d.c.length;
          item.GetCurrentStatus.call(trackCOunt-1).then(function(status){
            $("#status").text(status[0]);
            var items;
           
           
          })
      })
     .catch(function(e) {
        // There was an error! Handle it.
        console.log(e);
      });

      });
     // var seizureSource= "pragma solidity ^0.4.2;  contract Seizure {string holder; string why; string who; uint StartTimestamp; uint endTimestamp; address[] public itemAddress; address owner; function Seizure() {owner = msg.sender; StartTimestamp = now; } function createItemContract(string SeizureId , string desc){Item item = new Item(SeizureId,desc); itemAddress.push(item); } function GetItems() returns (address[]){return itemAddress; } }";
     // web3.eth.compile.solidity(seizureSource, function(error, seizureCompiled){
       // console.log(error);
        //console.log(seizureCompiled);
      //  seizureABI = seizureCompiled['<stdin>:Seizure'].info.abiDefinition;
      //  seizureContract = web3.eth.contract(seizureABI);
    //    seizureCode = seizureCompiled['<stdin>:Seizure'].code;

     // }); 
    });
  },
  createSeizure: function()
  {
    Seizure.new().then(function(instance) {
      // `instance` is a new instance of the abstraction.
      // If this callback is called, the deployment was successful.
      document.getElementById("contractAddress").value=instance.address;
    }).catch(function(e) {
      // There was an error! Handle it.
      console.log(e);
    });
    
  Seizure.deployed().then(function(f){
    document.getElementById("contractAddress").value=f.address;
  });
      
    
  },

  addItem: function()
  {
    var contractAddress = document.getElementById("contractAddress").value;
    var item = document.getElementById("itemName").value;
    var toAddress = document.getElementById("toAddress").value;
    var deployedSeizure = Seizure.deployed();
    Seizure.deployed().then(function(f){
      f.createItemContract(item,item,{from:account,to:toAddress}).then(function(data){
      
      console.log(data);
      var items = f.GetItems.call().then(function(data){
      
        document.getElementById("items").value = data;
      });
    }).catch(function(e) {
      // There was an error! Handle it.
      console.log(e);
    });
  });
  },
  getItem: function()
  {
    var contractAddress = document.getElementById("contractAddress").value;
    var item = document.getElementById("itemName").value;
    var toAddress = document.getElementById("toAddress").value;
    var deployedItem = Item.at(contractAddress);
    Item.deployed().then(function(f){
      console.log(f.address);
      var items = f.GetItem.call().then(function(data){
      
        document.getElementById("items").value = data;
      }).catch(function(e) {
        // There was an error! Handle it.
        console.log(e);
      });
    });
  },
  AddTrack: function()
  {
    var contractAddress = document.getElementById("contractAddress").value;
    var status = document.getElementById("status").value;
    var location = document.getElementById("location").value;
    var toAddress = document.getElementById("toAddress").value;
    var deployedItem = Item.at(contractAddress);
    Item.deployed().then(function(f){
      f.AddTrack(status,location,{from:account,to:contractAddress}).then(function(data){
        
        console.log(data);
      
      }).catch(function(e) {
        // There was an error! Handle it.
        console.log(e);
      });
    });
  },
  GetCurrentStatus: function(){

    var contractAddress = document.getElementById("contractAddress").value;
    var f ;
    Item.deployed().then(function(instance){f=instance;});
    var count = 0;
    f.GetTrailCount.call().then(function(data){
      
        count = data.s;
        f.GetCurrentStatus.call(count-1).then(function(data){
          
            count = data.s;
          }).catch(function(e) {
            // There was an error! Handle it.
            console.log(e);
          });
      }).catch(function(e) {
        // There was an error! Handle it.
        console.log(e);
      });
      
    
    
  },


  addNewLocation: function()
  {
    var contractAddress = document.getElementById("contractAddress").value;
    var deployedFoodSafe = foodSafeContract.at(contractAddress);
    var locationId = document.getElementById("locationId").value;
    var locationName = document.getElementById("locationName").value;
    var locationSecret = document.getElementById("secret").value;
    var passPhrase = document.getElementById("passPhrase").value;
    var encryptedSecret = CryptoJS.AES.encrypt(locationSecret,passPhrase).toString();
    deployedFoodSafe.AddNewLocation(locationId, locationName, encryptedSecret, function(error){
      console.log(error);
    })
  },
  getCurrentLocation: function()
  {
    var contractAddress = document.getElementById("contractAddress").value;
    var deployedFoodSafe = foodSafeContract.at(contractAddress);
    var passPhrase = document.getElementById("passPhrase").value;
    deployedFoodSafe.GetTrailCount.call(function (error, trailCount){
      deployedFoodSafe.GetLocation.call(trailCount-1, function(error, returnValues){
        document.getElementById("locationId").value= returnValues[1];
        document.getElementById("locationName").value = returnValues[0];
        var encryptedSecret = returnValues[4];
        var decryptedSecret = CryptoJS.AES.decrypt(encryptedSecret, passPhrase).toString(CryptoJS.enc.Utf8);
        document.getElementById("secret").value=decryptedSecret;
      })
    })    
  }
};

window.addEventListener('load', function() {
  if (typeof web3 !== 'undefined') {
    console.warn("Using web3 detected from external source.  If using MetaMask, see the following link. Feel free to delete this warning. :) http://truffleframework.com/tutorials/truffle-and-metamask")
    window.web3 = new Web3(web3.currentProvider);
  } else {
    console.warn("No web3 detected. Falling back to http://localhost:8545. You should remove this fallback when you deploy live, as it's inherently insecure. Consider switching to Metamask for development. More info here: http://truffleframework.com/tutorials/truffle-and-metamask");
    // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
    window.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
  }
  App.start();
});
